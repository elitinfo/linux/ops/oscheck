# oscheck.sh

Az OS ellenőrző script 2 fajta disztribúciót ismer jelenleg:
- Ubuntu LTS
- SuSE Linux Enterprise Server

## Előfeltételek

1. A cél hostokra kell SSH kulcs alapú bejelentkezési lehetőség arról a hostról, ahol az oscheck.sh-t futtatjuk.
2. Az oscheck.sh scriptben fel kell sorolni az ellenőrzendő hostokat a 2 for ciklusban. IP cím is megadható (a táblázatban a cél gépek hostneve jelenik meg, nem az a név, ahogyan kapcsolódtunk)

## Megjelenített adatok:

### Ubuntu esetében
|                   |                                                   |                                     |
| ----------------- |---------------------------------------------------| ------------------------------------|
| Hostname          | Hostnév                                           | $HOSTNAME                           |
| OS release        | OS disztribúció (release és SP szint)             | /etc/os-release: $PRETTY_NAME       |
| Kernel version    | Linux kernelverzió                                | uname -r                            |
| Last patch update | Utolsó OS frissítés adatai (dátum, patch, verzió) | /var/log/dpkg.log                   |
| Available patches | Jelenleg rendelkezésreálló patchek száma          | apt-check --human-readable          |

### SLES esetében
|                   |                                                   |                                     |
| ----------------- |---------------------------------------------------| ------------------------------------|
| Hostname          | Hostnév                                           | $HOSTNAME                           |
| OS release        | OS disztribúció (release és SP szint)             | /etc/os-release: $PRETTY_NAME       |
| Kernel version    | Linux kernelverzió                                | uname -r                            |
| Last patch update | Utolsó OS frissítés adatai (dátum, patch, verzió) | rpm -qa --last                      |
| Available patches | Jelenleg rendelkezésreálló patchek száma          | zypper list-patches --with-optional |

## Kiemelés

Az utolsó elérhető Release/SP szinten lévő rendszereket a script zöld színnel kiemeli a táblázatban, függetlenül a patch szinttől.

## Minta

![](images/oscheck_sh_example.png)
