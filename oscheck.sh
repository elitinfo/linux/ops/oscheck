#!/bin/bash

LATEST_UBUNTU="Ubuntu 20.04.4 LTS"
LATEST_SLES="SUSE Linux Enterprise Server 15 SP3"

# Table header

echo
echo OS patch report on `date`
echo
printf "%-15s%-40s%-32s%-103s%-30s\n" "Hostname" "OS release" "Kernel version" "Last patch update" "Available patches"
printf "=%.0s" {1..250}
echo

# Ubuntu systems

for u in ubuhost1 ubuhost2 ubuhost3
do
    ssh $u 'printf "%-15s" $HOSTNAME; . /etc/os-release; printf "%-40s" "$PRETTY_NAME"; printf "%-30s" `uname -r`; echo -n "  "; printf "%-100s" "$(tail -n 1 /var/log/dpkg.log)"; echo -n "   "; printf "%s" "$(/usr/lib/update-notifier/apt-check --human-readable | head -n 1)"' | GREP_COLOR='01;32' egrep --color '.*'"$LATEST_UBUNTU"'.*|$'
done

# SLES systems

for h in sleshost1 sleshost2 sleshost3 sleshost4 sleshost5 sleshost6 
do
    ssh $h 'printf "%-15s" $HOSTNAME; . /etc/os-release; printf "%-40s" "$PRETTY_NAME"; printf "%-30s" `uname -r`; echo -n "  "; printf "%-100s" "$(rpm -qa --last | head -n 1)"; echo -n "   "; zypper list-patches --with-optional 2>/dev/null | tail -n 2 | head -n 1 | grep -v Warning | grep -v Refreshing ' | GREP_COLOR='01;32' egrep --color '.*'"$LATEST_SLES"'.*|$' 
done
